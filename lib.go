package kvparser

import (
	"fmt"
	"strings"
)

func FromKVStrings(kvstrings []string) (map[string]string, error) {
	m := make(map[string]string)

	for _, s := range kvstrings {
		kv := strings.SplitN(s, "=", 2)

		if len(kv) < 2 {
			return m, fmt.Errorf("string '%v' cannot be parsed as key=value", s)
		}

		m[kv[0]] = kv[1]
	}

	return m, nil
}
