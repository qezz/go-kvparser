package kvparser

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestVecOfStringsToHashMap(t *testing.T) {
	testcases := []struct {
		name string
		in   []string
		out  map[string]string
	}{
		{
			name: "Key Value strings parse demo",
			in: []string{
				"key=value",
				"longKey=Long Value With whitespaces",
			},
			out: map[string]string{
				"key":     "value",
				"longKey": "Long Value With whitespaces",
			},
		},
	}

	for _, testcase := range testcases {
		t.Run(testcase.name, func(t *testing.T) {
			actual, err := FromKVStrings(testcase.in)
			if err != nil {
				t.Fatalf("Error while parsing key-value strings: %v", err)
			}

			if !cmp.Equal(actual, testcase.out) {
				t.Fatalf("Actual is not equal with expected: %v != %v", actual, testcase.out)
			}
		})
	}
}
